package main

import (
	"flag"
	"fmt"
	"net/http"
)

var port = flag.String("p", ":80", "port")
var dir = flag.String("d", ".", "directory to serve")

func main() {
	flag.Parse()
	fs := http.FileServer(http.Dir(*dir))
	http.Handle("/", http.StripPrefix("/", fs))
	fmt.Println("Serving on port ", *port)
	http.ListenAndServe(*port, nil)
}
